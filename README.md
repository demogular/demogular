README
======

1. git clone git://gitorious.org/demogular/demogular.git demogular
2. cd demogular
3. npm install
4. bower install
5. editor components/font-awesome/less/font-awesome.less # ligne 24: @FontAwesomePath:   "components/font-awesome/font";
6. components/less.js/bin/lessc -sm -su bootstrap.less styles.css # facultatif, voir 8a.
7. node_modules/bricks/bin/bricks --log access.log &
8. x-www-browser http://localhost:8080/index.html # voir 6. ci-haut
8a. ou x-www-browser http://localhost:8080/index-less.html
9. tail -f access.log

npm? bower?
-----------
Si vous n'avez pas bower ou npm, il faudra peut-être aussi installer npm (node package manager) ainsi que node lui-même.

    sudo apt-get install python-software-properties python g++ make # ou software-properties-common au lieu de python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js # add-apt-repository n'est pas disponible dans debian squeeze mais il se trouve dans debian wheezy
    sudo apt-get update
    sudo apt-get install nodejs # npm est inclus dans nodejs, tandis que node est un projet complètement différent

Maintenant vous pouvez installer bower:

    sudo npm install bower -g

Retournez ensuite à l'étape 3.

